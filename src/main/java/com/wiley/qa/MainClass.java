package com.wiley.qa;

import com.wiley.qa.checkers.SpecificChecker;
import com.wiley.qa.marks.TitleProvider;
import com.wiley.qa.pages.HomePage;

public class MainClass {

    @TitleProvider(HomePage.class)
    public static void main(String[] args) {
        HomePage homePage = new HomePage();
        SpecificChecker checker = new SpecificChecker();
        new SpecificChecker().isDisplayd("Locator 2")
                .isDisplayd("");
    }

    public static void main2(String[] args) {
        HomePage homePage = new HomePage();
        SpecificChecker checker = new SpecificChecker();
        new SpecificChecker().isDisplayd("Locator 1")
                .isDisplayd("");
    }
}

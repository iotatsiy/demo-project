package com.wiley.qa.pages;

import com.wiley.qa.marks.Title;

public class ContactPage {
    @Title("Locator 1")
    public String locator1 = "I'm locator 1";

    @Title("Locator 2")
    public String locator2 = "I'm locator 2";
}
